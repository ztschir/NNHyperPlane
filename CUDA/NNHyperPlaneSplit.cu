#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <cmath>
#include <algorithm>
#include <cuda_profiler_api.h>

//#include <cub/cub.cuh>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <thrust/execution_policy.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/binary_search.h>

#include <tuple>
#include <thrust/tuple.h>
#include <iostream>
#include <thrust/unique.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/sort.h>

#include <chrono>

#define USE_GPU
//#define NUM_DIM 2
#define NUM_DIM 8

#define Real double
#define Point Real


//using Clock = std::chrono::steady_clock;
using Clock = std::chrono::high_resolution_clock;
using std::chrono::time_point;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
//using namespace std::literals::chrono_literals;


struct Node{
  bool isLeaf;
  Node *left;
  Node *right;
  int leafPointsInd;
  int numberOfPoints;
};


void generatePoints(Point * points, int numberOfPoints, int numberOfDimensions){
  //srand (time(NULL));
  srand(0);
  for(int i = 0; i < numberOfPoints; i++)
    for(int j = 0; j < numberOfDimensions; j++)
      points[i*numberOfDimensions + j] = ((double) rand() / (RAND_MAX));
}

__global__
void findCentroidDevice(Point * points, Point * center, int numberOfPoints, int numberOfDimensions){
  int i = blockIdx.x;
  if(i < numberOfDimensions){
    Real sum = 0.0;
    for(int j = 0; j < numberOfPoints; j++){
      sum += points[j*numberOfDimensions + i];
    }
    center[i] = sum / numberOfPoints;
  }
}

void findCentroid(Point * points, Point * center, int numberOfPoints, int numberOfDimensions){

  
  #ifdef USE_GPU
  findCentroidDevice<<<numberOfDimensions, 1>>>(points, center, numberOfPoints, numberOfDimensions);
  #else
  
  for(int i = 0; i < numberOfDimensions; i++){
    Real sum = 0.0;
    for(int j = 0; j < numberOfPoints; j++)
      sum += points[j*numberOfDimensions + i];
    center[i] = sum / numberOfPoints;
  }
  #endif
}

Real findSingleDistance(Point * firstPoint, Point * secondPoint, int numberOfDimensions){
  Real sum = 0.0;
  for(int i = 0; i < numberOfDimensions; i++){
    sum += (firstPoint[i] - secondPoint[i]) * (firstPoint[i] - secondPoint[i]);
  }
  return sqrt(sum);
}

__global__
void findDistanceDevice(Point * firstPoints, Point * diffPoint, Real * distance, int numberOfPoints, int numberOfDimensions){
  int i = blockIdx.x;
  Real sum = 0.0;
  Real firstPoint;
  
  extern __shared__ Real diffP[];
  diffP[i%numberOfDimensions] = diffPoint[i%numberOfDimensions];

  for(int j = 0; j < numberOfDimensions; j++){
    firstPoint = firstPoints[i*numberOfDimensions + j] - diffPoint[j];
    firstPoint *= firstPoint;
    sum += firstPoint;
  }
  distance[i] = sqrt(sum);
}

void findDistance(Point * firstPoints, Point * diffPoint, Real * distance, int numberOfPoints, int numberOfDimensions){
  
  #ifdef USE_GPU
  findDistanceDevice<<<numberOfPoints, 1, numberOfDimensions*sizeof(Real)>>>(firstPoints, diffPoint,
                                                                             distance, numberOfPoints,
                                                                             numberOfDimensions);
    
  #else
  
  for(int i = 0; i < numberOfPoints; i++)
    distance[i] = findSingleDistance(&firstPoints[numberOfDimensions*i], diffPoint, numberOfDimensions);
  
  #endif
}

int maxPointIndex(Real * distances, int numberOfPoints){

  #ifdef USE_GPU
  return thrust::distance(distances,thrust::max_element(thrust::device, distances, distances + numberOfPoints));

  #else
  int ind = 0;
    
  Real max = 0.0;
  
  for(int i = 0; i < numberOfPoints; i++){
    if(distances[i] > max){
      max = distances[i];
      ind = i;
    }
  }
  return ind;  
  #endif
}

void pointDiff(Point * point1, Point * point2, Point * diff, int numberOfDimensions){

  #ifdef USE_GPU
  thrust::transform(thrust::device, point1, point1 + numberOfDimensions,
                    point2, diff, thrust::minus<Real>());
  #else
  
  for(int i = 0; i < numberOfDimensions; i++)
    diff[i] = point1[i] - point2[i];

  #endif
}


__global__
void projectPointsDevice(Real * distance1, Real * distance2, Real * distance,
                         Real * t, int numberOfPoints, int numberOfDimensions){
  int i = blockIdx.x;
  Real d1 = distance1[i];
  Real d2 = distance2[i];
  Real recDistance = 1 / (*distance);
  t[i] = ((d1*d1 - d2*d2) * recDistance + (*distance)) * 0.5 * recDistance;
}



void projectPoints(Point * points, Point * point2, Point * point1, Real * alpha, int numberOfPoints, int numberOfDimensions){

  #ifdef USE_GPU
  Real * distance1;
  Real * distance2;
  Real * distance;
  
  cudaMalloc((void **)&distance1, numberOfPoints*sizeof(Real));
  cudaMalloc((void **)&distance2, numberOfPoints*sizeof(Real));
  cudaMalloc((void **)&distance, sizeof(Real));  

  findDistance(points, point1, distance1, numberOfPoints, numberOfDimensions);
  findDistance(points, point2, distance2, numberOfPoints, numberOfDimensions);
  findDistance(point1, point2, distance,  1, numberOfDimensions);

  projectPointsDevice<<<numberOfPoints, 1>>>(distance1, distance2, distance,
                                             alpha, numberOfPoints, numberOfDimensions);
  
  cudaFree(distance1);
  cudaFree(distance2);
  cudaFree(distance);
  
  #else
  Real * distance1 = new Real[numberOfPoints];
  Real * distance2 = new Real[numberOfPoints];
  Real * distance = new Real[numberOfPoints];  
  
  findDistance(points, point1, distance1, numberOfPoints, numberOfDimensions);
  findDistance(points, point2, distance2, numberOfPoints, numberOfDimensions);
  findDistance(point1, point2, distance, 1, numberOfDimensions);

  for(int i = 0; i < numberOfPoints; i++){
    alpha[i] = ((distance1[i]*distance1[i] - distance2[i]*distance2[i]) / distance[0]) + distance[0];
    alpha[i] /= 2.0;
    alpha[i] /= distance[0];
  }
  
  delete distance1;
  delete distance2;
  delete distance;
  
  #endif
}


__global__
void assignBuckets(Real * vec, int n, int B, Real *minimum, Real *maximum, int * bucket, unsigned int * counter){
  int block = blockIdx.x;
  int thread = threadIdx.x;
  int i = block*blockDim.x + thread;
  Real slope = ((Real)(B - 1))/(*maximum - *minimum);
  extern __shared__ unsigned int localCounter[];
  localCounter[thread] = counter[thread];
  __syncthreads();
  
  if(i < n){
    int tempBucket = (int) (slope * (vec[i] - *minimum));
    bucket[i] = tempBucket;
    atomicInc((unsigned int *)&localCounter[tempBucket],n);
  }
  __syncthreads();
  atomicAdd((unsigned int *)&counter[thread], localCounter[thread]);
}


__global__
void findMinMax(Real * minimum, Real * maximum, int kBucket, int B){
  Real slope = ((Real)(B - 1))/(*maximum - *minimum);
  *minimum = max(*minimum, *minimum+ kBucket/slope);
  *maximum = min(*maximum, *minimum + 1/slope);
}


struct contained_bucket{
  int kBucket;
  contained_bucket(int kB) : kBucket(kB) {};
  __host__ __device__
  bool operator()(const int x){
    return (x == kBucket);
  }
};

struct not_contained_bucket{
  int kBucket;
  not_contained_bucket(int kB) : kBucket(kB) {};
  __host__ __device__
  bool operator()(const int x){
    return (x != kBucket);
  }
};


void medianDevice(Real * alpha, Real *medianPoint, int numberOfPoints, int k, int numberOfBuckets){
  //printf("Started...\n");
  thrust::device_vector<int> bucket(numberOfPoints,0);
  thrust::device_vector<unsigned int> counter(numberOfBuckets, 0);
  thrust::device_vector<unsigned int> scan_counter(numberOfBuckets, 0);

  while(1){

    thrust::fill(counter.begin(), counter.end(), 0);
    thrust::pair<Real *, Real *> result =
      thrust::minmax_element(thrust::device, alpha, alpha + numberOfPoints);
    if(thrust::equal(thrust::device,result.first,result.first+1,result.second)){
      //printf("Exit 1\n");
      cudaMemcpy(medianPoint, thrust::raw_pointer_cast(&result.second[0]),
        sizeof(Real), cudaMemcpyDeviceToDevice);
      return;
    }

    // Assign Buckets
    int grid = std::max((int)numberOfPoints/numberOfBuckets,1);
    
    assignBuckets<<<grid, numberOfBuckets,
      numberOfBuckets*sizeof(unsigned int)>>>(alpha,
                                              numberOfPoints, numberOfBuckets,
                                              result.first, result.second,
                                              thrust::raw_pointer_cast(bucket.data()),
                                              thrust::raw_pointer_cast(counter.data()));
    // FindK Bucket

    thrust::inclusive_scan(thrust::device, counter.begin(), counter.end(), scan_counter.begin());
    int kBucket = std::min((int)thrust::distance(scan_counter.begin(),
                                   thrust::lower_bound(thrust::device,
                                                       scan_counter.begin(),
                                                       scan_counter.end(),
                                                       k, thrust::less<unsigned int>())), numberOfBuckets-1);


    unsigned int kCount = counter[kBucket];
    int sum;
    if(kBucket < numberOfBuckets-1)
      sum = scan_counter[kBucket+1];
    else
    sum = scan_counter[kBucket];
    
    if(kCount == 1){
      //printf("Exit 2\n");
      cudaMemcpy(medianPoint,
                 &alpha[thrust::distance(bucket.begin(),
                                         thrust::find(bucket.begin(), bucket.end(), kBucket))],
                                         sizeof(Real), cudaMemcpyDeviceToDevice);
      return;
    }
    else{
      //printf("Got here 2\n");    
      if(kBucket != 0)
        k = k - sum + kCount;
      thrust::device_vector<Real> newAlphaDevice(kCount,0);      
      thrust::copy_if(thrust::device, alpha, alpha + numberOfPoints,
                      bucket.begin(), newAlphaDevice.begin(), contained_bucket(kBucket));

      //printf("Got here 3\n");
      //Phase II
      {
        thrust::fill(counter.begin(), counter.end(), 0);
        thrust::fill(scan_counter.begin(), scan_counter.end(), 0);
        bucket.resize(kCount, 0);
        
        thrust::pair<thrust::device_vector<Real>::iterator,
                     thrust::device_vector<Real>::iterator>
          midResult = thrust::minmax_element(thrust::device,
                                             newAlphaDevice.begin(),
                                             newAlphaDevice.end());

        //thrust::host_vector<Real> test(newAlphaDevice.begin(), newAlphaDevice.end());
        
        //printf("Host Alpha Values: \n");
        //for(int i = 0; i < numberOfPoints; i++)
        //  printf("%f \n", test[i]);

        if(thrust::equal(midResult.first,midResult.first + 1, midResult.second)){
          //printf("Exit 3\n");
          cudaMemcpy(medianPoint, thrust::raw_pointer_cast(&midResult.second[0]),
                     sizeof(Real), cudaMemcpyDeviceToDevice);
          return;
        }
        
        grid = std::max((int)kCount/numberOfBuckets,1);
        assignBuckets<<<grid, numberOfBuckets,
          numberOfBuckets*sizeof(unsigned int)>>>(thrust::raw_pointer_cast(newAlphaDevice.data()),
                                                  kCount, numberOfBuckets,
                                                  thrust::raw_pointer_cast(&midResult.first[0]),
                                                  thrust::raw_pointer_cast(&midResult.second[0]),
                                                  thrust::raw_pointer_cast(bucket.data()),
                                                  thrust::raw_pointer_cast(counter.data()));
        // FindK Bucket
        thrust::inclusive_scan(thrust::device, counter.begin(), counter.end(), scan_counter.begin());
        kBucket = std::min((int)thrust::distance(scan_counter.begin(),
                                   thrust::lower_bound(thrust::device,
                                                       scan_counter.begin(),
                                                       scan_counter.end(),
                                                       k, thrust::less<unsigned int>())), numberOfBuckets-1);
        kCount = counter[kBucket];        

        if(kBucket < numberOfBuckets-1){
          sum = scan_counter[kBucket+1];
        }
        else{
          sum = scan_counter[kBucket];
        }


        while(kBucket > 1 && thrust::equal(thrust::device, midResult.second,
                                           midResult.second + 1,
                                           midResult.first,
                                           thrust::greater<Real>())){

          findMinMax<<<1,1>>>(thrust::raw_pointer_cast(&midResult.first[0]),
                              thrust::raw_pointer_cast(&midResult.second[0]),
                              kBucket, numberOfBuckets);
                                                    
          k = k - sum + kCount;

          if(thrust::equal(thrust::device,midResult.second, midResult.second + 1,
                           midResult.first, thrust::greater<Real>())){

            //printf("Got here 6\n");    
            thrust::remove_if(thrust::device, newAlphaDevice.begin(), newAlphaDevice.end(),
                              bucket.begin(), not_contained_bucket(kBucket));
            bucket.resize(kCount, 0);
            thrust::fill(counter.begin(), counter.end(), 0);
            thrust::fill(scan_counter.begin(), scan_counter.end(), 0);      

            grid = std::max((int)kCount/numberOfBuckets,1);
            
            assignBuckets<<<grid, numberOfBuckets,
              numberOfBuckets*sizeof(unsigned int)>>>(thrust::raw_pointer_cast(newAlphaDevice.data()),
                                                      kCount, numberOfBuckets,
                                                      thrust::raw_pointer_cast(&midResult.first[0]),
                                                      thrust::raw_pointer_cast(&midResult.second[0]),
                                                      thrust::raw_pointer_cast(bucket.data()),
                                                      thrust::raw_pointer_cast(counter.data()));
            
            // FindK Bucket           
            thrust::inclusive_scan(thrust::device, counter.begin(), counter.end(), scan_counter.begin());
            kBucket = std::min((int)thrust::distance(scan_counter.begin(),
                                       thrust::lower_bound(thrust::device,
                                                           scan_counter.begin(),
                                                           scan_counter.end(),
                                                           k, thrust::less<unsigned int>())), numberOfBuckets-1);
            kCount = counter[kBucket];
            if(kBucket < numberOfBuckets-1)
              sum = scan_counter[kBucket+1];
            else
              sum = scan_counter[kBucket];

          }
          else{
            //printf("Exit 4\n");
            cudaMemcpy(medianPoint, thrust::raw_pointer_cast(&midResult.second[0]),
                       sizeof(Real), cudaMemcpyDeviceToDevice);
            return;
          }
        }

        //printf("Exit 5\n");
        cudaMemcpy(medianPoint,
                   thrust::raw_pointer_cast(
                       &newAlphaDevice[thrust::distance(bucket.begin(),
                                                        thrust::find(bucket.begin(),
                                                                     bucket.end(), kBucket))]),
                   sizeof(Real), cudaMemcpyDeviceToDevice);
       
        return;
      }
        
    }

  }
}             
    

template <typename Iterator>
void median(Iterator begin, Iterator end, Real * mid) {

  #ifdef USE_GPU
  int numberOfPoints = thrust::distance(begin, end);
  int numberOfBuckets = 1024;
  medianDevice(begin, mid, numberOfPoints, numberOfPoints/2, numberOfBuckets);

  #else
  int diff = end - begin;
  Real * tempAlpha = new Real[end - begin];
  std::copy(begin, end, tempAlpha);
  begin = tempAlpha;
  end = tempAlpha + diff;
  
  Iterator middle = begin + (end - begin) / 2;
   
  std::nth_element(begin, middle, end);
  
  if ((end - begin) % 2 != 0) {
    *mid = *middle;
    return;
  } else {
    Iterator lower_middle = std::max_element(begin, middle);
    *mid = (*middle + *lower_middle) / 2.0;
    return;
  }
  #endif
}


/*void partition(points, numberOfPoints, numberOfDimensions, minNumberOfPoints){
  Point * center = findCentroid(points, numberOfPoints, numberOfDimensions);
  Real * distance = findDistance(points, center, numberOfPoints, numberOfDimensions);
  Point * Point1 = &points[maxPointIndex(distance, numberOfPoints)*numberOfDimensions];
  
  distance = findDistance(points, Point1, numberOfPoints, numberOfDimensions);
  Point * Point2 = &points[maxPointIndex(distance, numberOfPoints)*numberOfDimensions];
  Point * diffPoint = pointDiff(Point1, Point2, numberOfDimensions);
  Real * alpha = projectPoints(points, diffPoint, center, numberOfPoints, numberOfDimensions);
  Real mid = median(alpha, alpha + numberOfPoints);

  int numberOfPointsLeft = (int)std::floor((float)numberOfPoints/2.0);
  int numberOfPointsRight = numberOfPoints - numberOfPointsLeft;
  int j = 0;
  Real * temp = new Real[numberOfDimensions];
  for(int i = 0; i < numberOfPointsLeft; i++){
    if(alpha[i] > mid){
      for(; j < numberOfPointsRight; j++){
        if(alpha[numberOfPointsLeft+j] < mid){
          memcpy(temp, &points[i*numberOfDimensions], numberOfDimensions*sizeof(Real));
          memcpy(&points[i*numberOfDimensions], &points[(numberOfPointsLeft+j)*numberOfDimensions],
                 numberOfDimensions*sizeof(Real));
          memcpy(&points[(numberOfPointsLeft+j)*numberOfDimensions], temp, numberOfDimensions*sizeof(Real));
          j++;
          break;
        }
      }
    }
  }
  delete temp;
  delete center;
  delete distance;
  delete diffPoint;
  delete alpha;
  }*/


/*Node * hyperplaneSplit(Point * points, int numberOfPoints, int numberOfDimensions, int minNumberOfPoints){

  int numberOfLeafNodes = 0;
  Point * previousPoints = NULL;
  Point * currentPoints = points;
  int previousNumberOfPoints = numberOfPoints;  
  int currentNumberOfPoints = numberOfPoints;
  while(numberOfLeafNodes < numberOfPoints/minNumberOfPoints){

    partition(currentPoints, currentNumberOfPoints, numberOfDimensions, minNumberOfPoints);

    if(currentNumberOfPoints/2 < minNumberOfPoints){
      // Left Node
      Node * leaf = new Node();
      leaf->isLeaf = true;
      leaf->leafPoints = points;
      leaf->numberOfPoints = currentNumberOfPoints;

      // Right Node
      Node * leaf = new Node();
      leaf->isLeaf = true;
      leaf->leafPoints = points + currentNumberOfPoints;
      leaf->numberOfPoints = previousNumberOfPoints - currentNumberOfPoints;
      
      numberOfLeafNodes += 2;
      continue;
    }
    
    
  }

  }*/

struct is_less_than{
  Real * mid;
  is_less_than(Real * middle) : mid(middle) {};
  __host__ __device__
  bool operator()(const Real x){
    return (x <= *mid);
  }
};

// Modified version of https://github.com/thrust/thrust/blob/master/examples/strided_range.cu
template <typename Iterator, typename thrust::iterator_difference<Iterator>::type stride>
class strided_range{
public:
  typedef typename thrust::iterator_difference<Iterator>::type difference_type;

  struct stride_functor : public thrust::unary_function<difference_type,difference_type>{
    __host__ __device__
    difference_type operator()(const difference_type& i) const{ 
      return stride * i;
    }
  };

  typedef typename thrust::counting_iterator<difference_type>                   CountingIterator;
  typedef typename thrust::transform_iterator<stride_functor, CountingIterator> TransformIterator;
  typedef typename thrust::permutation_iterator<Iterator,TransformIterator>     PermutationIterator;

  typedef PermutationIterator iterator;

  strided_range(Iterator first, Iterator last)
    : first(first), last(last) {}

  iterator begin(void) const{
    return PermutationIterator(first, TransformIterator(CountingIterator(0), stride_functor()));
  }

  iterator end(void) const{
    return begin() + ((last - first) + (stride - 1)) / stride;
  }

protected:
  Iterator first;
  Iterator last;
};

template<typename, typename>
struct append_to_type_seq { };

template<typename T, typename... Ts, template<typename...> class TT>
struct append_to_type_seq<T, TT<Ts...> >{
  using type = TT<Ts..., T>;
};

template<typename T, unsigned int N, template<typename...> class TT>
struct repeat{
  using type = typename
    append_to_type_seq<T,
    typename repeat<T, N-1, TT>::type>::type;
};

template<typename T, template<typename...> class TT>
struct repeat<T, 0, TT>{
  using type = TT<>;
};

template<typename Tuple> struct std_to_thrust_tuple;
template<typename...T> struct std_to_thrust_tuple<std::tuple<T...> > {
  using type = thrust::tuple<T...>;
};

template<typename IteratorType, std::size_t stride>
class zipped_strided_range{
public:

  typedef typename strided_range<IteratorType, stride>::iterator SingleIterator;
  typedef typename repeat<SingleIterator, stride, std::tuple>::type StdIteratorTuple;
  typedef typename std_to_thrust_tuple<StdIteratorTuple>::type IteratorTuple;
  typedef decltype(thrust::make_zip_iterator(IteratorTuple())) ZipIterator;

  zipped_strided_range(IteratorType first, IteratorType last) : first(first), last(last){
    assign<0>();
  }

  ZipIterator begin() const{
    return thrust::make_zip_iterator(begin_tuple);
  }

  ZipIterator end() const{
    return thrust::make_zip_iterator(end_tuple);
  }

protected:

  template <std::size_t index>
  void assign(typename std::enable_if< (index < stride) >::type* = 0){
    strided_range<IteratorType,stride> strided_range_iterator(first+index, last-(stride-1)+index);

    thrust::get<index>(begin_tuple) = strided_range_iterator.begin();
    thrust::get<index>(end_tuple) = strided_range_iterator.end();
    assign<index+1>();
  }

  template <std::size_t index>
  void assign(typename std::enable_if< (index == stride) >::type* = 0){
  }

  IteratorType first;
  IteratorType last;

  IteratorTuple begin_tuple;
  IteratorTuple end_tuple;
};

Node * hyperplaneSplit(Point * points, int numberOfPoints, int numberOfDimensions, int minNumberOfPoints, Point * origin){

  if(numberOfPoints/2 < minNumberOfPoints){
    Node * leaf = new Node();
    leaf->isLeaf = true;
    leaf->leafPointsInd = points - origin;
    leaf->numberOfPoints = numberOfPoints;
    return leaf;
  }

  #ifdef USE_GPU

  thrust::device_vector<Point> d_center(numberOfDimensions);
  thrust::device_vector<Real> d_distance(numberOfPoints);
  thrust::device_vector<Real> d_alpha(numberOfPoints);
  thrust::device_vector<Point> d_diffPoint(numberOfDimensions);
  thrust::device_vector<Real> d_mid(1);

  Point * center    = thrust::raw_pointer_cast(d_center.data());
  Real * distance   = thrust::raw_pointer_cast(d_distance.data());
  Real * alpha      = thrust::raw_pointer_cast(d_alpha.data());
  Point * diffPoint = thrust::raw_pointer_cast(d_diffPoint.data());
  Real * mid        = thrust::raw_pointer_cast(d_mid.data());
 
  #else

  Point * center = new Point[numberOfDimensions];
  Real * distance = new Real[numberOfPoints];
  Real * alpha = new Real[numberOfPoints];
  Point * diffPoint = new Real[numberOfDimensions];
  Real * mid = new Real[1];  

  #endif

  Node * node = new Node();
  node->isLeaf = false;

  findCentroid(points, center, numberOfPoints, numberOfDimensions);
  findDistance(points, center, distance, numberOfPoints, numberOfDimensions);
  int point1Ind = maxPointIndex(distance, numberOfPoints)*numberOfDimensions;
  findDistance(points, points + point1Ind, distance, numberOfPoints, numberOfDimensions);
  int point2Ind = maxPointIndex(distance, numberOfPoints)*numberOfDimensions;
  pointDiff(points+point1Ind, points+point2Ind , diffPoint, numberOfDimensions);
  projectPoints(points, diffPoint, center, alpha, numberOfPoints, numberOfDimensions);
  
  median(alpha, alpha + numberOfPoints, mid);
  int numberOfPointsLeft = (int)std::floor((float)numberOfPoints/2.0);
  int numberOfPointsRight = numberOfPoints - numberOfPointsLeft;
  #ifdef USE_GPU


  //Real tempMid;
  //cudaMemcpy(&tempMid, &mid, sizeof(Real), cudaMemcpyDeviceToHost);
  //thrust::host_vector<Real> tempMid(d_mid);
  //printf("mid: %f\n", tempMid[0]);

  zipped_strided_range<Real*, NUM_DIM> zipped(points, points + numberOfPoints*numberOfDimensions);

  thrust::partition(thrust::device,
                    zipped.begin(),
                    zipped.end(),
                    alpha,
                    is_less_than(mid));

  
  d_center.clear();
  d_distance.clear();
  d_alpha.clear();
  d_diffPoint.clear();
  d_mid.clear();  
  
  thrust::device_vector<Point>().swap(d_center);
  thrust::device_vector<Real>().swap(d_distance);
  thrust::device_vector<Real>().swap(d_alpha);
  thrust::device_vector<Point>().swap(d_diffPoint);
  thrust::device_vector<Real>().swap(d_mid);
  
  #else
  
  int j = 0;
  Real * temp = new Real[numberOfDimensions];
  for(int i = 0; i < numberOfPointsLeft; i++){
    if(alpha[i] <= *mid){
      for(; j < numberOfPointsRight; j++){
        if(alpha[numberOfPointsLeft+j] > *mid){
          memcpy(temp, &points[i*numberOfDimensions], numberOfDimensions*sizeof(Real));
          memcpy(&points[i*numberOfDimensions], &points[(numberOfPointsLeft+j)*numberOfDimensions],
                 numberOfDimensions*sizeof(Real));
          memcpy(&points[(numberOfPointsLeft+j)*numberOfDimensions], temp, numberOfDimensions*sizeof(Real));
          j++;
          break;
        }
      }
    }
  }

  delete temp;
  delete center;
  delete distance;
  delete diffPoint;
  delete alpha;
  delete mid;

  #endif

  node->left = hyperplaneSplit(points, numberOfPointsLeft, numberOfDimensions, minNumberOfPoints, origin);
  node->right = hyperplaneSplit(&points[numberOfPointsLeft*numberOfDimensions], numberOfPointsRight,
                                numberOfDimensions, minNumberOfPoints, origin);
  
  return node;
}


void traverseTree(Point * points, Node * node, int numberOfDimensions){
  if(!node->isLeaf){
    traverseTree(points, node->left, numberOfDimensions);
    traverseTree(points, node->right, numberOfDimensions);
  }
  else{
    printf("\n");
    for(int i = 0; i < node->numberOfPoints * numberOfDimensions; i += 2){
      printf("%f %f\n", points[node->leafPointsInd + i], points[node->leafPointsInd + i + 1]);
    }
  }
}




int main(int argc, char **argv){

  //int numberOfPoints = 1048576;
  //int numberOfPoints = 262144;
  int numberOfPoints = 16384;
  //int numberOfPoints = 16;
  //int numberOfDimensions = 2;
  int numberOfDimensions = NUM_DIM;
  
  //int numberOfNeighbors = 4;
  int minNumberOfPoints = 4;
  
  Point * points = new Point[numberOfPoints*numberOfDimensions];
  generatePoints(points, numberOfPoints, numberOfDimensions);
  #ifdef USE_GPU

  time_point<Clock> start_copy_to = Clock::now();  
  thrust::device_vector<Real> devicePoints(points, points + (numberOfDimensions*numberOfPoints));
  time_point<Clock> end_copy_to = Clock::now();

  time_point<Clock> start_run = Clock::now();    
  Node * topLevelNode = hyperplaneSplit(thrust::raw_pointer_cast(devicePoints.data()),
                                        numberOfPoints, numberOfDimensions,
                                        minNumberOfPoints, thrust::raw_pointer_cast(devicePoints.data()));
  time_point<Clock> end_run = Clock::now();

  thrust::copy(devicePoints.begin(), devicePoints.end(), points);

  milliseconds diff_copy_to = duration_cast<milliseconds>(end_copy_to - start_copy_to);
  milliseconds diff_run = duration_cast<milliseconds>(end_run - start_run);
  std::cout << "Copy to GPU: "<< diff_copy_to.count() << " ms" << std::endl;
  std::cout << "Runtime: "<< diff_run.count() << " ms" << std::endl;
  

  #else
  time_point<Clock> start_run = Clock::now();    
  Node * topLevelNode = hyperplaneSplit(points, numberOfPoints, numberOfDimensions, minNumberOfPoints, points);
  time_point<Clock> end_run = Clock::now();
  milliseconds diff_run = duration_cast<milliseconds>(end_run - start_run);
  std::cout << "Runtime: "<< diff_run.count() << " ms" << std::endl;
  
  #endif
  //traverseTree(points, topLevelNode, numberOfDimensions);
  cudaProfilerStop();
  delete points;
  return 0;
}
