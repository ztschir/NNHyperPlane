function NNHyperplaneSplit()

    %NUMBER_OF_POINTS     = 1024;
    NUMBER_OF_POINTS = 16;
    NUMBER_OF_DIMENSIONS = 2;
    NUMBER_OF_NEIGHBORS  = 2;
    %MIN_NUMBER_OF_POINTS = 16;
    MIN_NUMBER_OF_POINTS = 4;
    
    %points = rand(NUMBER_OF_POINTS, NUMBER_OF_DIMENSIONS + 1);
    points = [0 0.840188 0.394383; 
              0 0.783099 0.798440; 
              0 0.911647 0.197551; 
              0 0.335223 0.768230; 
              0 0.277775 0.553970; 
              0 0.477397 0.628871; 
              0 0.364784 0.513401; 
              0 0.952230 0.916195; 
              0 0.635712 0.717297; 
              0 0.141603 0.606969; 
              0 0.016301 0.242887; 
              0 0.137232 0.804177; 
              0 0.156679 0.400944; 
              0 0.129790 0.108809; 
              0 0.998924 0.218257; 
              0 0.512932 0.839112];
    points(:,1) = 1:NUMBER_OF_POINTS;
    
    neighborsLeaf = {};
    neighborsLeaf = hyperplaneSplit(points, neighborsLeaf,...
                    MIN_NUMBER_OF_POINTS);    
    
    nearestNeighborsIndex = zeros(NUMBER_OF_POINTS, NUMBER_OF_NEIGHBORS);
    % direct search

    for node = 1:size(neighborsLeaf,2)
        for q = 1:size(neighborsLeaf{node},1)
            pointIndex = neighborsLeaf{node}(q,1);
            nearestNeighborsIndex(pointIndex, :) = directSearch(q, neighborsLeaf{node}, NUMBER_OF_NEIGHBORS);
        end
    end
    
    %plot results if NUMBER_OF_DIMENSIONS == 2
    if NUMBER_OF_DIMENSIONS == 2
        printTwoDimHyperPlane(neighborsLeaf)
    end
    
    
    
    % if NUMBER_OF_DIMENSIONS == 2
    %    scatter(points(:,2),points(:,3), '.');
    %    hold on;
    %    scatter(points(1,2),points(1,3), '.', 'sizeData', 200);
    %    p = 1;
    %    for i = nearestNeighborsIndex(1,:)
    %        NNGroupX(p) = points(i,2);
    %        NNGroupY(p) = points(i,3);
    %        p = p + 1;
    %    end
    %    scatter(NNGroupX,NNGroupY, 'x', 'sizeData', 200);
    %    hold off;
    % end
    
    
end

function indexList = directSearch(q, neighborLeaf, ...
                                  numberOfNeighbors)
        
    point = neighborLeaf(q,2:end);
    distance = pdist2(neighborLeaf(:,2:end), point);
    
    sortedPoints = sortrows([neighborLeaf(:,1),distance],2);
    
    indexList = sortedPoints(2:numberOfNeighbors+1,1)';
end

function neighborsLeaf = hyperplaneSplit(points, neighborsLeaf, minNumberOfPoints)


    NUMBER_OF_POINTS = size(points, 1);
    NUMBER_OF_DIMENSIONS = size(points, 2);    
    pointValues = points(:,2:NUMBER_OF_DIMENSIONS);
    if(NUMBER_OF_POINTS <= minNumberOfPoints || ...
       NUMBER_OF_POINTS/2 < minNumberOfPoints)
        neighborsLeaf{end+1} = points;
        return;
    end
    
    center = findCentroid(pointValues)
    distance = pdist2(pointValues, center)';
    
    [val, point1idx] = max(distance);
    point1 = pointValues(point1idx,:);
    
    distance = pdist2(pointValues, point1)';
    [val, point2idx] = max(distance);
    point2 = pointValues(point2idx,:);
    
    point = point1 - point2;
    
    % line generated from center to point
    for i = 1:NUMBER_OF_POINTS
        alpha(i) = projectPoint(pointValues(i,:), point, center);
    end
    
    alpha
    
    mid = median(alpha)
    
    j = 1;
    k = 1;
    pointsLeft = zeros(floor(NUMBER_OF_POINTS/2),NUMBER_OF_DIMENSIONS);
    pointsRight = zeros(ceil(NUMBER_OF_POINTS/2),NUMBER_OF_DIMENSIONS);
        
    for i = 1:NUMBER_OF_POINTS
        if alpha(i) < mid
            pointsLeft(j,:) = points(i,:);
            j = j + 1;
        else
            pointsRight(k,:) = points(i,:);
            k = k + 1;
        end
    end
    
    neighborsLeaf = hyperplaneSplit(pointsLeft, neighborsLeaf, minNumberOfPoints);
    neighborsLeaf = hyperplaneSplit(pointsRight, neighborsLeaf, minNumberOfPoints);
end

function center = findCentroid(M)

    NUMBER_OF_POINTS     = size(M,1);
    NUMBER_OF_DIMENSIONS = size(M,2);
    center = zeros(1, NUMBER_OF_DIMENSIONS);

    for i = 1:NUMBER_OF_DIMENSIONS
        center(1,i) = sum(M(:,i))./NUMBER_OF_POINTS;
    end

end

function alpha = projectPoint(P, V2, V1)
    d1 = pdist2(P, V1);
    d2 = pdist2(P, V2);
    d  = pdist2(V1, V2);
    t = ((d1*d1 - d2*d2)/d) + d;
    t = t/2;
    alpha = t/d;
end

function printTwoDimHyperPlane(neighborsLeaf)
    x = neighborsLeaf{1}(:,2);
    y = neighborsLeaf{1}(:,3);
    scatter(x,y,'.', 'sizedata', 200);
    [x y]
    hold all
    for i = 2:size(neighborsLeaf,2)
        x = neighborsLeaf{i}(:,2);
        y = neighborsLeaf{i}(:,3); 
        [x y]        
        scatter(x,y,'.', 'sizedata', 200);
    end
    hold off 
end





